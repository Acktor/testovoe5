<h1>
Тестовое
</h1>
<span>
Небольшая документация по тому как пользоваться. Все комиты находятся в ветке master</span>
<h2>
HOW TO
</h2>
<h3>
Install docker
</h3>
<ul>
<li>Скачать архив</li>
<li>Разорхивировать в удобном для вас месте</li>
<li>Освободить порт 88</li>
<li>Вызвать команду <i>docker-compose up</i></li>
</ul>
<p>В качестве БД используется postgres. Для удобства разработки был прокинут порт на хост машину. Внутри контейра используется порт 5432. Для хост-машины порт 54321</p>
<h3>
Install api
</h3>
<span>
После запуска докера будет создан каталог <i>/testovoe</i> в директории где был развернут докер. Для полноценной работы приложения нужно перейти в него и выполнить следующее:</span>
<ul>
<li>git clone git@gitlab.com:Acktor/testovoe5.git .</li>
<li>composer install</li>
<li>Перейти в bash контейнер с fpm</li>
<li>Перейти в директорию /var/www/article</li>
<li>cp .env.example .env</li>
<li>configure <i>.env</i></li>
<li>php artisan migrate</li>
</ul>
<span>В случае если последние 2 команды не сработали, нужно либо проверить конфигурацию .env</span>
<h2>Api docs</h2>
<h3>Image</h3>
<span>Изображения по умолчанию храняться в директории с проектов по пути ./public/images</span>
<p>Для корректной работы нужно убедиться правильно ли выставлены доступы</p>
<h4>Upload Image <b>GET</b> <i>/api/image/upload</i></h4>
<span>Принимает в себя форму с полем image в котором содержиться изображение</span>

```javascript
{
    "name": "1637172111.png",
    "path": "/var/www/article/public/images",
    "full_path": "//var/www/article/public/images/1637172111.png",
    "updated_at": "2021-11-17T18:01:51.000000Z",
    "created_at": "2021-11-17T18:01:51.000000Z",
    "id": 2
}
```

<h4>Get info Image <b>GET</b> <i>/api/image/info/{id}</i></h4>
<span>Выводит информацию об изображении. Где {id} является id с изображением</span>
<p>Response json:</p>

```javascript
{
    "id": 1,
    "name": "default_image.jpg",
    "path": "images",
    "full_path": "images/default_image.jpg",
    "deleted_at": null,
    "created_at": null,
    "updated_at": null
}
```

<h4>Render Image <b>GET</b> <i>/api/image/render/{id}</i></h4>
<span>Выводит изображение. Где {id} является id с изображением</span>
<h4>Delete Image <b>DELETE</b> <i>/api/image/render/{id}</i></h4>
<span>Удаляет изображение и меняет изображение на изображение с id = 1, везде в статьях где оно упоминается. Параметр в апи {id} является id с изображением</span>
<h3>Article</h3>
<h4>Create Article <b>POST</b> <i>/api/article/</i></h4>
<span>Создает статью где в теле запроса принимает информацию о статье</span>
<p>
Body request json:
</p>

```javascript
{
    "title" : "Название статьи",
    "description" : "Описание",
    "image_id" : 1, //ID изображения, изображения храняться в таблице images
    "type_id" : 1,  //ID типа статьи, где 1 = новость, типы статьи храняться в таблице article_types
    "public_date" : "2021-11-17 17:36:00",  //Дата публикации в формате Datetime
    "is_active" : true  //bool значение
}
```

<p>Response json:</p>

```javascript
{
    "title" : "Название статьи",
    "description" : "Описание",
    "image_id" : 1, //ID изображения, изображения храняться в таблице images
    "type_id" : 1,  //ID типа статьи, где 1 = новость, типы статьи храняться в таблице article_types
    "public_date" : "2021-11-17 17:36:00",  //Дата публикации в формате Datetime
    "is_active" : true  //bool значение
    "updated_at": "2021-11-17T17:38:14.000000Z",
    "created_at": "2021-11-17T17:38:14.000000Z",
    "id": 3
}
```
<h4>Update Article <b>PUT</b> <i>/api/article/{id}</i></h4>
<span>Обновляет статью где в теле запроса принимает информацию о статье. Параметр в запросе {id} является id статьи</span>
<p>
Body request json:
</p>

```javascript
{
    "title" : "Название статьи",
    "description" : "Описание",
    "image_id" : 1, //ID изображения, изображения храняться в таблице images
    "type_id" : 1,  //ID типа статьи, где 1 = новость, типы статьи храняться в таблице article_types
    "public_date" : "2021-11-17 17:36:00",  //Дата публикации в формате Datetime
    "is_active" : true  //bool значение
}
```

<p>Response json:</p>

```javascript
{
    "title" : "Название статьи",
    "description" : "Описание",
    "image_id" : 1, //ID изображения, изображения храняться в таблице images
    "type_id" : 1,  //ID типа статьи, где 1 = новость, типы статьи храняться в таблице article_types
    "public_date" : "2021-11-17 17:36:00",  //Дата публикации в формате Datetime
    "is_active" : true  //bool значение
    "updated_at": "2021-11-17T17:38:14.000000Z",
    "created_at": "2021-11-17T17:38:14.000000Z",
    "id": 3
}
```
<h4>GET Article <b>GET</b> <i>/api/article/{id}</i></h4>
<span>Выводит информации о статье. Параметр в запросе {id} является id статьи</span>
<p>Response json:</p>

```javascript
{
    "title" : "Название статьи",
    "description" : "Описание",
    "image_id" : 1, //ID изображения, изображения храняться в таблице images
    "type_id" : 1,  //ID типа статьи, где 1 = новость, типы статьи храняться в таблице article_types
    "public_date" : "2021-11-17 17:36:00",  //Дата публикации в формате Datetime
    "is_active" : true  //bool значение
    "updated_at": "2021-11-17T17:38:14.000000Z",
    "created_at": "2021-11-17T17:38:14.000000Z",
    "id": 3
    "image": {  //Информаци о изображении
        "id": 1,
        "name": "default_image.jpg",
        "path": "images",
        "full_path": "/images/default_image.jpg",
        "deleted_at": null,
        "created_at": null,
        "updated_at": null
    },
    "type": {   //Информаци о типе статьи
        "id": 1,
        "title": "Новости",
        "slug": "news",
        "deleted_at": null,
        "created_at": null,
        "updated_at": null
    }
}
```

<h4>Delete Article <b>DELETE</b> <i>/api/article/{id}</i></h4>
<span>Удаляет статью. Параметр в запросе {id} является id статьи</span>
<p>Response status is <i>204</i></p>
<h4>GET List of Articles <b>GET</b> <i>/api/articles</i></h4>
<span>Выводит список статей</span>
<p>Response json:</p>

```javascript
[
    {
        "id": 2,
        "title": null,
        "description": null,
        "image_id": null,
        "type_id": 1,
        "public_date": "2021-11-17 17:36:01",
        "is_active": null,
        "deleted_at": null,
        "created_at": "2021-11-17T17:36:00.000000Z",
        "updated_at": "2021-11-17T17:36:00.000000Z"
    },
    //...
    {
        "id": 10,
        "title": null,
        "description": null,
        "image_id": null,
        "type_id": 1,
        "public_date": "2021-11-17 17:36:01",
        "is_active": null,
        "deleted_at": null,
        "created_at": "2021-11-17T17:36:00.000000Z",
        "updated_at": "2021-11-17T17:36:00.000000Z"
    },
]
```
<h4>GET List of Articles by ArticleType <b>GET</b> <i>/api/articles/{type_slug}</i></h4>
<span>Выводит список статей которые принадлежат к заданному типу статьи. Где <i>{type_slug}</i> является типом статьи. Например <i>news</i></span>
<p>Response json:</p>

```javascript
[
    {
        "id": 2,
        "title": null,
        "description": null,
        "image_id": null,
        "type_id": 1,
        "public_date": "2021-11-17 17:36:01",
        "is_active": null,
        "deleted_at": null,
        "created_at": "2021-11-17T17:36:00.000000Z",
        "updated_at": "2021-11-17T17:36:00.000000Z"
    },
    //...
    {
        "id": 10,
        "title": null,
        "description": null,
        "image_id": null,
        "type_id": 1,
        "public_date": "2021-11-17 17:36:01",
        "is_active": null,
        "deleted_at": null,
        "created_at": "2021-11-17T17:36:00.000000Z",
        "updated_at": "2021-11-17T17:36:00.000000Z"
    },
]
```

<h2>
MORE
</h2>
<p>
POSTMAN: https://app.getpostman.com/join-team?invite_code=212e91e5a29fcce69138057ec8f9dec9&ws=f4d067b1-8953-4390-ac2f-cbc150065b89
</p>
<p>А еще я забыл про тесты <3</p>
