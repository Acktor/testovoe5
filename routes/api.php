<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'article'], function () {
    Route::get('/{id}', [ArticleController::class, 'item']);
    Route::post('/', 'ArticleController@save');
    Route::put('/{id}', 'ArticleController@save');
    Route::delete('/{id}', 'ArticleController@delete');
});

Route::group(['prefix' => 'articles'], function () {
    Route::get('/', 'ArticleController@list');
    Route::get('/{article_slug}', 'ArticleController@list');
});

Route::group(['prefix' => 'image'], function () {
    Route::get('/render/{id}', 'ImageController@render');
    Route::post('/upload', 'ImageController@upload');
    Route::delete('/delete/{id}', 'ImageController@delete');
    Route::get('/info/{id}', 'ImageController@item');
});
