<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    protected $guarded = ['id'];

    use HasFactory, SoftDeletes;

    public function image(){
        return $this->belongsTo(Image::class);
    }

    public function type(){
        return $this->belongsTo(ArticleType::class);
    }
}
