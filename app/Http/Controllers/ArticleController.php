<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function item(int $id)
    {
        $article = Article::where('id',$id)->with(['image', 'type'])->first();

        return response()->json($article, 200);
    }

    /**
     * @param Request $request
     * @param string|null $article_slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request, string $article_slug = null)
    {
        $articles_query = Article::query();

        if (!empty($article_slug)){
            $articles_query = $articles_query->whereHas('type', function ($query) use ($article_slug){
                $query->where('slug', 'like', $article_slug);
            });
        }

        $articles = $articles_query->get();

        return response()->json($articles, 200);
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, $id = null)
    {
        $data = $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'image_id' => 'required|max:255',
            'type_id' => 'required|max:255',
            'public_date' => 'required|max:255',
            'is_active' => 'required|max:255',
        ]);

        if (!empty($id)){
            $article = Article::where('id',$id)->first();
        } else {
            $article = new Article();
        }

        $article->fill($data)->save();

        return response()->json($article, 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $article = Article::find($id)->delete();

        return response()->json($article, 204);
    }
}
