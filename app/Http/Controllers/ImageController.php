<?php

namespace App\Http\Controllers;

use App\Http\Services\ImageService;
use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image_info = ImageService::upload($request->image);

        $image = new Image();

        $image->fill($image_info)->save();

        return response()->json($image, 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function render(Request $request, $id)
    {
        $image = Image::where('id', $id)->first();

        return response()->file($image->full_path);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function item(Request $request, $id)
    {
        $image = Image::where('id', $id)->first();

        return response()->json($image, 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $id)
    {
        $image = Image::find($id)->delete();   //delete from directory in model at beforeSave method

        return response()->json($image, 204);
    }
}
