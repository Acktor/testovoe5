<?php


namespace App\Http\Services;


class ImageService
{

    /**
     * @param $image
     * @return array
     */
    public static function upload($image)
    {
        $imageName = time() . '.' . $image->extension();

        $path = public_path('images');

        $image->move($path, $imageName);

        return [
            'name' => $imageName,
            'path' => $path,
            'full_path' => $path . '/' . $imageName
        ];
    }

    /**
     * @param $image_full_path
     * @return bool
     */
    public static function delete($image_full_path)
    {
        unlink($image_full_path);

        return true;
    }
}
